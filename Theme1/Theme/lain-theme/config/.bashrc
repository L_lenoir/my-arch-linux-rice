#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
alias update='sudo pacman -Syyu' 
alias vivaldi='vivaldi-stable'
alias spotify='spotify-launcher'
alias eclipse='flatpak run org.eclipse.Java'
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias eclipse='flatpak org.eclipse.Java'
alias spotify='flatpak com.spotify.Client'
alias switchlain='bash Documents/Theme/lain-theme/scripts/switchtheme.sh'
alias switchblame1='bash Documents/Theme/blame-theme1/scripts/switchtheme.sh'
PS1='[\u@\h \W]'
alias NUUH='echo "NU UH";'
setxkbmap fr
eval "$(starship init bash)"
alias ll='ls -lABhX'
wal -q -e -n -i ~/Images/wallpaper/lain_cross.jpeg 
