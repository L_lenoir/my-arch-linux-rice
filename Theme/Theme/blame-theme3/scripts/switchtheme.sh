#!/bin/sh
echo 'test' &
rm ~/.config/bspwm/bspwmrc &
rm ~/.bashrc &
rm ~/.config/polybar/config.ini &
cp ~/Documents/Theme/blame-theme3/config/bspwmrc ~/.config/bspwm/ &
cp ~/Documents/Theme/blame-theme3/config/.bashrc ~/ &
cp ~/Documents/Theme/blame-theme3/config/config.ini ~/.config/polybar/
