#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ipserv='sudo ip addr add 192.168.15.254/24 dev enp2s0 && sudo ip route add default via 192.168.15.0 && sudo ip link set dev enp2s0 up'
alias iprasp='sudo ip addr add 192.168.15.1/24 dev enp2s0 && sudo ip route add default via 192.168.15.0 && sudo ip link set dev enp2s0 up'
alias dhcpd='sudo docker run -it --rm --init --net host -v "$(pwd)/data/":/data networkboot/dhcpd enp2s0'
alias switch-mac='sudo ip link set dev wlp3s0 down && sudo ip link set dev enp2s0 down && sudo macchanger -r wlp3s0 && sudo macchanger -r enp2s0 && sudo ip link set dev wlp3s0 up && sudo ip link set dev enp2s0 up'
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
alias update='sudo pacman -Syyu' 
alias vivaldi='vivaldi-stable'
alias spotify='spotify-launcher'
alias eclipse='flatpak run org.eclipse.Java'
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias eclipse='flatpak org.eclipse.Java'
alias spotify='flatpak com.spotify.Client'
alias switchlain='bash ~/Documents/Theme/lain-theme/scripts/switchtheme.sh'
alias switchblame1='bash ~/Documents/Theme/blame-theme1/scripts/switchtheme.sh'
alias switchgunnm='bash ~/Documents/Theme/gunnm-theme/scripts/switchtheme.sh'
PS1='[\u@\h \W]'
alias NUUH='echo "NU UH";'
setxkbmap fr
eval "$(starship init bash)"
alias ll='ls -lABhX'
wal -q -e -n -i ~/Images/wallpaper/gunnm1.jpg 
