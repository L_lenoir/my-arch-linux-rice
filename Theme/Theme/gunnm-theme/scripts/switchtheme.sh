#!/bin/sh
echo 'test' &
rm ~/.config/bspwm/bspwmrc &
rm ~/.bashrc &
rm ~/.config/polybar/config.ini &
cp ~/Documents/Theme/gunnm-theme/config/bspwmrc ~/.config/bspwm/ &
cp ~/Documents/Theme/gunnm-theme/config/.bashrc ~/ &
cp ~/Documents/Theme/gunnm-theme/config/config.ini ~/.config/polybar/
